import pandas as pd
from sqlalchemy import create_engine


def read_dump_file(DumpStage,DumpName):
    df=pd.DataFrame()
    if(DumpStage.upper()=='SOURCE'):
        df=pd.read_csv("Dumps\\SOURCE\\"+DumpName+".csv",dtype=str)
    elif(DumpStage.upper()=='TARGET'):
        df = pd.read_csv("Dumps\\TARGET\\" + DumpName + ".csv",dtype=str)

    return df

def read_database_table(DumpStage,tableName,sql):
    df=pd.DataFrame()
    DBConnectionDetailsDF = pd.read_excel("Driver.xlsx", sheet_name="DB_Connections")
    if(DumpStage.upper()=='SOURCE'):
        DBConnectionDetailsDF = DBConnectionDetailsDF[DBConnectionDetailsDF['RESOURCE_TYPE'] == 'SOURCE']
    elif(DumpStage.upper()=='TARGET'):
        DBConnectionDetailsDF = DBConnectionDetailsDF[DBConnectionDetailsDF['RESOURCE_TYPE'] == 'TARGET']
    DATABASE_TYPE=""
    HOST=""
    PORT=""
    DB_NAME=""
    USERNAME=""
    PASSWORD=""
    DB_LibraryName=""
    for index, row in DBConnectionDetailsDF.iterrows():
        DATABASE_TYPE = str(row['DATABASE_TYPE'])
        HOST = str(row['HOST'])
        PORT = str(row['PORT'])
        DB_NAME = str(row['DB/SERVICE_NAME'])
        USERNAME = str(row['USERNAME'])
        PASSWORD = str(row['PASSWORD'])
    if(DATABASE_TYPE=="ORACLE"):
        DB_LibraryName='oracle+cx_oracle'
    elif(DATABASE_TYPE=="PostgerSQL"):
        DB_LibraryName = 'postgresql+psycopg2'
    elif(DATABASE_TYPE=="MYSQL"):
        DB_LibraryName = 'mysql+mysqlconnector'

    connection_string = DB_LibraryName+'://'+USERNAME+':'+PASSWORD+'@'+HOST+':'+PORT+'/'+DB_NAME
    alchemyEngine = create_engine(connection_string)
    dbConnection = alchemyEngine.connect()
    if(sql=="NULL"):
        df = pd.read_sql_table(tableName, dbConnection)
    else:
        df = pd.read_sql(sql, dbConnection)

    return df.applymap(str)

def convert(seconds):
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return "%d:%02d:%02d" % (hour, minutes, seconds)

def splitColumns(df,sourceColumn,TargetColumns):
    TargetColumnsListMain = TargetColumns.split("(")
    TargetColumnsList=TargetColumnsListMain[0].split(",")
    seperater = TargetColumnsListMain[1].replace(")", "")
    if(seperater!=""):
        df[TargetColumnsList] = df[sourceColumn].str.split(seperater, expand=True)
    else:
        df[TargetColumnsList] = df[sourceColumn].str.split(' ', expand=True)

    columns = [sourceColumn]
    df.drop(columns, inplace=True, axis=1)
    return df

def dateformater(df,sourcecolumn,TargetFormat):
    TargetFormatList = TargetFormat.split("(")
    TargetColumn=TargetFormatList[0]
    TargetFormater=TargetFormatList[1].replace(")","")
    df[sourcecolumn] = pd.to_datetime(df[sourcecolumn])
    df[sourcecolumn] = df[sourcecolumn].dt.strftime(TargetFormater)
    #'%m-%d-%Y'
    #'%d-%m-%Y'
    #'%B %d, %Y'
    df[sourcecolumn] = df[sourcecolumn].astype(str)
    if(sourcecolumn!=TargetColumn):
        df=df.rename(columns={sourcecolumn:TargetColumn})
    return df

def mergeColumns(df,sourceColumns,TargetColumns):
    sourceColumnsList=sourceColumns.split(",")
    TargetColumnsList=TargetColumns.split("(")
    seperater=TargetColumnsList[1].replace(")","")
    if(seperater !=""):
        df[TargetColumnsList[0]] = df[sourceColumnsList[0]] + "," +  df[sourceColumnsList[1]].astype(str)
    else:
        df[TargetColumnsList[0]] = df[sourceColumnsList[0]] + seperater + df[sourceColumnsList[1]].astype(str)

    columns = sourceColumnsList
    df.drop(columns, inplace=True, axis=1)
    return df

def country_to_codes(df,sourceColumn,TargetColumn):
    CountryCodesDF=pd.read_excel("Data.xlsx",sheet_name="COUNTRY_CODES")
    codes_dict = dict(zip(CountryCodesDF.COUNTRY_NAME, CountryCodesDF.COUNTRY_CODE))
    df[sourceColumn] = df[sourceColumn].map(codes_dict)
    if (sourceColumn != TargetColumn):
        df = df.rename(columns={sourceColumn: TargetColumn})
    return df

def dataframe_difference(df1,df2,which=None):
    diff_df=pd.DataFrame()
    comparision_df=df1.merge(df2,indicator=True,how='outer')
    if which is None:
        diff_df=comparision_df[comparision_df['_merge']!='both']
    else:
        diff_df=comparision_df[comparision_df['_merge']==which]
    return diff_df

