import time
import PySimpleGUI as sg
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=UserWarning)
import Utilities
from Utilities import read_dump_file

print("########### Execution Started ###########")
start_time = time.time()
DriverSheetDF=pd.read_excel("Driver.xlsx",sheet_name="Driver")
DriverSheetDF=DriverSheetDF[DriverSheetDF['RUN_CONTROL'] == 'YES']
DriverSheetDF.fillna("NULL",inplace=True)
print("Total Tables Selected For Recon: "+str(DriverSheetDF.shape[0]))

column_names = ["TableName", "Source_Count_Before_Remove_Dup","Target_Count_Before_Remove_Dup","Source_Count_After_Remove_Dup","Target_Count_After_Remove_Dup","Source_Dup_count","Target_Dup_count","Matching","Data_NotMatching","Source_Delta","Target_Delta"]
overview = pd.DataFrame(columns=column_names)

for index, row in DriverSheetDF.iterrows():
    Source_DataDF=pd.DataFrame()
    Target_DataDF=pd.DataFrame()

    MappingSheetName=str(row.MAPPINGSHEET)
    print("############################# "+MappingSheetName+" #############################")
    ResourceType=str(row.RESOURCE)
    print("Resource Type: "+ResourceType)
    SourceTableName=str(row.SOURCE_TABLE_NAME)
    print("Source Table/Dump Name: "+SourceTableName)
    TargetTableName=str(row.TARGET_TABLE_NAME)
    Source_SQL = str(row.SOURCE_SQL)
    Target_SQL = str(row.TARGET_SQL)
    print("Target Table/Dump Name: "+TargetTableName)

    if(ResourceType.upper() == 'DUMP'):
        Source_DataDF=read_dump_file('SOURCE',SourceTableName)
        Target_DataDF=read_dump_file('TARGET',TargetTableName)
    elif(ResourceType.upper() == 'DATABASE'):

        Source_DataDF= Utilities.read_database_table('SOURCE', SourceTableName,Source_SQL)
        Target_DataDF = Utilities.read_database_table('TARGET', TargetTableName,Target_SQL)
    else:
        print("ERROR: Resource Type Not Provided For The Table "+MappingSheetName)

    Source_DataDF_Count_Before_Drop_Dup=Source_DataDF.shape[0]
    Target_DataDF_Count_Before_Drop_Dup=Target_DataDF.shape[0]
    # print("Source row count before remove duplicates: "+str(Source_DataDF_Count_Before_Drop_Dup))
    # print("Target row count before remove duplicates: "+str(Target_DataDF_Count_Before_Drop_Dup))
    #Remove Duplicates and save
    sourceDupsDf=Source_DataDF[Source_DataDF.duplicated(keep='first')]
    targetDupsDf = Target_DataDF[Target_DataDF.duplicated(keep='first')]
    sourceDupsDf.to_csv("Results\\Duplicates\\Source\\"+MappingSheetName+"_duplicates.csv")
    targetDupsDf.to_csv("Results\\Duplicates\\Target\\" + MappingSheetName + "_duplicates.csv")
    Source_DataDF=Source_DataDF.drop_duplicates(keep='first')
    Target_DataDF = Target_DataDF.drop_duplicates(keep='first')
    Source_DataDF_Count_After_Drop_Dup=Source_DataDF.shape[0]
    Target_DataDF_Count_After_Drop_Dup = Target_DataDF.shape[0]
    # print("Source row count after remove duplicates: " + str(Source_DataDF_Count_After_Drop_Dup))
    # print("Target row count after remove duplicates: " + str(Target_DataDF_Count_After_Drop_Dup))
    sourceDup_Count=str(Source_DataDF_Count_Before_Drop_Dup - Source_DataDF_Count_After_Drop_Dup)
    targetDup_Count=str(Target_DataDF_Count_Before_Drop_Dup - Target_DataDF_Count_After_Drop_Dup)
    # print("Total source duplicates: "+sourceDup_Count)
    # print("Total target duplicates: "+targetDup_Count)

    #Apply Transofrmation Logics On Source Data
    mappingSheetDF=pd.read_excel("MappingSheet.xlsx",sheet_name=MappingSheetName).fillna('NULL')
    TransormationLogicsDF = mappingSheetDF[mappingSheetDF['TRANSORMATION LOGIC'] != 'NULL']
    PrimaryKeysDF=mappingSheetDF[mappingSheetDF['PRIMARYKEY'] == 'Y']
    PrimaryKeysList=PrimaryKeysDF["TARGET COLUMNS"].to_list()
    if (TransormationLogicsDF.shape[0] > 0):
        for index, row in TransormationLogicsDF.iterrows():
            transformation_logic = row['TRANSORMATION LOGIC']
            logicList = str(transformation_logic).split(":")
            print("T/F Logic: " + str(transformation_logic))
            Source_logic_column = logicList[0]
            logic_function = logicList[1]
            Target_logic_column = logicList[2]

            RunLogic_Function = getattr(Utilities, logic_function)
            Source_DataDF=RunLogic_Function(Source_DataDF, Source_logic_column, Target_logic_column)
    #Rename column names if target has different name for non logic columns
    NonTransormationLogicsDF = mappingSheetDF[mappingSheetDF['TRANSORMATION LOGIC'] == 'NULL']
    if (NonTransormationLogicsDF.shape[0] > 0):
        for index, row in NonTransormationLogicsDF.iterrows():
            source_column = str(row['SOURCE COLUMNS'])
            target_column = str(row['TARGET COLUMNS'])
            if (source_column != target_column):
                Source_DataDF = Source_DataDF.rename(columns={source_column: target_column})
    # print(Source_DataDF.head())
    # Source_DataDF.to_csv("result.csv", index=False)
    NotMatchingData= Utilities.dataframe_difference(Source_DataDF, Target_DataDF)
    MatchingData=Utilities.dataframe_difference(Source_DataDF, Target_DataDF,'both')
    NotMatchingData = NotMatchingData.rename(columns={'_merge': 'Difference_status'})
    if(NotMatchingData.shape[0]>0):
        NotMatchingData['Difference_status'] = NotMatchingData['Difference_status'].str.replace('left_only','Source_only')
        NotMatchingData['Difference_status'] = NotMatchingData['Difference_status'].str.replace('right_only','Target_only')
        NotMatchingData_SourceOnly=NotMatchingData[NotMatchingData['Difference_status']=='Source_only']
        NotMatchingData_TargetOnly =NotMatchingData[NotMatchingData['Difference_status']=='Target_only']
        NotMatchingData_SourceOnly_primaryKey=NotMatchingData_SourceOnly[PrimaryKeysList]
        NotMatchingData_TargetOnly_primaryKey=NotMatchingData_TargetOnly[PrimaryKeysList]
        NotMatchingData_primaryKey = Utilities.dataframe_difference(NotMatchingData_SourceOnly_primaryKey, NotMatchingData_TargetOnly_primaryKey)
        if(NotMatchingData_primaryKey.shape[0]<1):
            NotMatchingData['Difference_status'] = NotMatchingData['Difference_status'].str.replace('Source_only','Source_Data_Mismatch')
            NotMatchingData['Difference_status'] = NotMatchingData['Difference_status'].str.replace('Target_only','Target_Data_Mismatch')
        else:
            NotMatchingData_primaryKey_SourceOnly = NotMatchingData_primaryKey[NotMatchingData_primaryKey['_merge'] == 'left_only']
            NotMatchingData_primaryKey_TargetOnly = NotMatchingData_primaryKey[NotMatchingData_primaryKey['_merge'] == 'right_only']
            NotMatchingData_primaryKey_SourceOnlyDF=NotMatchingData_primaryKey_SourceOnly[PrimaryKeysList[0]].to_list()
            NotMatchingData_primaryKey_TargetOnlyDF=NotMatchingData_primaryKey_TargetOnly[PrimaryKeysList[0]].to_list()
            NotMatchingData.loc[NotMatchingData[PrimaryKeysList[0]].isin(NotMatchingData_primaryKey_SourceOnlyDF),'Difference_status'] ='Sorce_only'
            NotMatchingData.loc[NotMatchingData[PrimaryKeysList[0]].isin(NotMatchingData_primaryKey_TargetOnlyDF), 'Difference_status'] = 'Target_only'
            NotMatchingData['Difference_status'] = NotMatchingData['Difference_status'].str.replace('Source_only','Source_Data_Mismatch')
            NotMatchingData['Difference_status'] = NotMatchingData['Difference_status'].str.replace('Target_only','Target_Data_Mismatch')

    NotMatchingData.to_csv("Results\\Defference\\"+MappingSheetName+"_difference.csv",index=False)

    MatchingDataCount=str(MatchingData.shape[0])
    NotMatchingDataCountDF=NotMatchingData[NotMatchingData['Difference_status']=='Source_Data_Mismatch']
    NotMatchingDataCoun=str(NotMatchingDataCountDF.shape[0])
    Source_DeltaDF=NotMatchingData[NotMatchingData['Difference_status']=='Sorce_only']
    Source_Delta_Count=str(Source_DeltaDF.shape[0])
    Target_DeltaDF = NotMatchingData[NotMatchingData['Difference_status'] == 'Target_only']
    Target_Delta_Count = str(Target_DeltaDF.shape[0])

    overview_DF=pd.DataFrame([[MappingSheetName,str(Source_DataDF_Count_Before_Drop_Dup),str(Target_DataDF_Count_Before_Drop_Dup),str(Source_DataDF_Count_After_Drop_Dup),str(Target_DataDF_Count_After_Drop_Dup),sourceDup_Count,targetDup_Count,MatchingDataCount,NotMatchingDataCoun,Source_Delta_Count,Target_Delta_Count]],columns =column_names)
    overview = pd.concat([overview,overview_DF.applymap(str)])
overview.to_csv("Results\\Overview.csv",index=False)
overview=overview.to_html()

# write html to file
text_file = open("Overview.html", "w")
text_file.write(overview)
text_file.close()

end_time = time.time()
TimeTaken= Utilities.convert(end_time - start_time)
print('Time Taken For Execution:'+ str(TimeTaken))

print("##################### Execution Completed in "+str(TimeTaken)+" ################")
sg.Popup('Execution completed in '+str(TimeTaken))

